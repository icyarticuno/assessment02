//install modules
const express = require("express");
const cors = require("cors");
const mysql = require("mysql");

//create instance of express
const app= express();

//enable CORS for all routes
app.use(cors());

const dbConfig = require('./dbconfig');
//create a connection pool for mysql
const pool= mysql.createPool(dbConfig);
//const SQL="select * from books where title like ?";
const SQL="select * from books where title like ? or author_firstname like ? or author_lastname like ? limit 10";
//select * from books where title like "%ella%" or author_lastname like "%Rowling%" or author_firstname like "%enid%";
//select * from books where title like "%Air%";


app.get("/search",(req,resp)=>{          
    console.log("Entered app.get");

    if(!req.query.title && !req.query.author){
    //if(!req.query.title || !req.query.author_firstname){
        console.log("OOps... missing the title and/or author");
        resp.status(400).send("Missing Search Term.");      //Bad request
    }


    title=`%${req.query.title}%`;
    author=`%${req.query.author}%`;

    if (!req.query.title){
    title="NULL";
    }
    if (!req.query.author){
        author="NULL";
        }

  

    //title=`%${req.query.title}%`;
    //author=`%${req.query.author}%`;
    console.log("titile in main: " , title);
    console.log("author in main: " , author);

    /*
    if (req.query.title=="%%"){
        console.log("Entered 'if(!req.query.title)=''");
        title="NULL";
    }

    if (req.query.author=="%%"){
        console.log("Entered 'if(!req.query.author)=''");
        author="NULL";
    }
    */

    pool.getConnection((err,conn)=>{
        if(err){
            resp.status(500).json({error:err});
            return;
        }
        conn.query(SQL, [title, author, author],
        // conn.query(SQL, [title],
            (err,result)=>{
                try{
                    if(err){
                        resp.status(400).json({error:err});  //Bad request
                        return;
                    }
                    console.log("title inside conn.query: ", title);
                    console.log("author inside conn.query: ", author);

                    resp.status(200);
                    resp.json(result);          //By typing this at browser, can see the result:
                                                //http://localhost:3000/search?title=WIND
                }
                finally{
                    conn.release();
                }
            })
        })
    })




    const SQL_BOOK_ID = 'select * from books where id = ?';
    //select * from books where id = 6;
    app.get('/search/:bookId', (req, resp) => {
        console.log(`book id = ${req.params.bookId}`);
    
        //selectByFilmId([ req.params.filmId])
        //    .then(result => {
        //        if (result.length)
        //            resp.status(200).json(result[0]);
        //        else
        //            resp.status(404).json({error: "Not found"});
        //    });
    
       pool.getConnection((error, conn) => {
           if (error) {
               resp.status(500).json({error: error}); return;
           }
           conn.query(SQL_BOOK_ID, [ req.params.bookId],
               (error, result) => {
                   try {
                       if (error) {
                           resp.status(400).json({error: error}); 
                           return;
                       }
                       if (result.length)
                           resp.status(200).json(result[0]);
                       else
                           resp.status(404).json({error: 'Not Found'});
                   } finally { conn.release() ;}
               }
           )
       })
    });
    
    



app.use('/',express.static(__dirname + "/images"));

//set port
const PORT = process.argv[2] || process.env.APP_PORT || 3000;

//Only start the application if the db is up
console.log('Pinging database...');
pool.getConnection((err, conn) => {
    if (err) {
        console.error('>Error: ', err);
        process.exit(-1);
    }

    conn.ping((err) => {
        if (err) {
            console.error('>Cannot ping: ', err);
            process.exit(-1);
        }
        conn.release();

        //Start application only if we can ping database
        app.listen(PORT, () => {
            console.log('Starting application on port %d', PORT);
        });
    });
});





    

