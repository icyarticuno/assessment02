import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';


import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BookService {

  constructor(private httpClient:HttpClient) { }

  /*
  getBook(title): Promise<any> {
    let qs = new HttpParams()
        .set('title', title)
        //.set('offset', config['offset'] || 0 );

      return (
        this.httpClient.get(
          'http://localhost:3000/search', { params: qs })
          .take(1).toPromise()
      );
  }
  */



  getBook(config={}): Promise<any> {
    let qs = new HttpParams()
        .set('title', config['title'] || "")
        .set('author',config['author'] || "");
        //.set('offset', config['offset'] || 0 );

      return (
        this.httpClient.get(
          'http://localhost:3000/search', { params: qs })
          .take(1).toPromise()
      );
  }


  getIndivBook(bookId: number): Promise<any> {
      return (
        this.httpClient.get(
          `http://localhost:3000/search/${bookId}'}`)
          .take(1).toPromise()
      );
  }

/*
  // GET /film/<filmId>
  getFilm(filmId: number): Promise<any> {
    return (
      this.httpClient.get(`${this.SERVER}/film/${filmId}`)
        .take(1).toPromise()
    );
  }
  */





  

}
