import { Component, ViewChild, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { BookService } from './book.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  //title = 'app';

   bookList=[];
   bookListSorted=[];
// bookListDisplayMax10=[];  //not in use since I use SQL command with limit 10


   formValidFlag:boolean=false;

    @ViewChild(NgForm) searchForm: NgForm;  //this stmt is required 

    constructor(private bookSvc:BookService){ }

    ngOnInit(){


    }

    search(searchForm:NgForm){    //Why need the underscore? seems to work without underscore too
        const title=this.searchForm.value['title'];
        console.log("title: " , title);
        
        const author=this.searchForm.value['author'];
        console.log("author: ", author);

        //To clear prev results if any.
        this.bookList=[];
        //this.bookListDisplayMax10=[];

        /*  Cater to search title only*/
        /*this.bookSvc.getBook(title)
          .then((result)=>{
            console.log(">>>results: ", result);
            this.bookList=result;
          })
          .catch((error)=>{
            console.log(">>> error: " , error);
          })
          }
          */
          
          

          if (title=="" && author==""){
              alert("Please Enter either title or author.");
          }
          else{
              this.bookSvc.getBook({title, author})
              .then((result)=>{
                  console.log(">>>results: ", result);
                  this.bookList=result;

                  //Returned Array length will always be 10 since the SQL command contains limit 10
                  //console.log("returned results array length: ", this.bookList.length);
                  

           
                  // SORT title in alphabetical order
                  /*
                  for(var i=0; i<9; i++){
                    if(this.bookList[i].title < this.bookList[i+1]){
                      this.bookListSorted[i]=this.bookList[i];
                    }else{
                      this.bookListSorted[i]=this.bookList[i+1];
                    }
                  }
                  */

                  //Try:

                  


                  console.log("bookListSorted: ", this.bookListSorted);

                  


                  /*
                //*********To display the first 10 books.
                //Actually Just use "limit in SQL command"....
                //Why did I spend so much time on this for loop...:(

                console.log(">> this.booklist.length: ", this.bookList.length);
           
                 if(this.bookList.length<10){
                    console.log(">>>>>> I am inside if booklist length <10");
                      for(var i=0; i<this.bookList.length; i++){
                            this.bookListDisplayMax10[i]=this.bookList[i];
                            console.log("hi");
                        } 
                  }else{
                      for(var i=0; i<10; i++){
                        this.bookListDisplayMax10[i]=this.bookList[i];
                        }
                    } 
                    
                    //  *********End of display first 10 books 
                    */
                    
                  })
                  
              .catch((error)=>{
                console.log(">>> error: " , error);
              })

          }

          
          
         
          }
  /*        
    compare(a,b){
      if (a.title < b.title)
      return -1;
      if (a.title > b.title)
      return 1;
      return 0;
  }
*/

      


    onSelect(b){
      console.log("A title is selected");
      
    }


    }





